package com.waracle.androidtest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {

    private Context context;
    // Can you think of a better way to represent these items???
    private ArrayList<Cake> cakes = new ArrayList<>();
    private ImageLoader mImageLoader;

    public MyAdapter(Context context, ArrayList<Cake> result) {
        this.context = context;
        mImageLoader = new ImageLoader();
        cakes = result;
    }

    @Override
    public int getCount() {
        return cakes.size();
    }

    @Override
    public Object getItem(int position) {
        return cakes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View root = inflater.inflate(R.layout.list_item_layout, parent, false);
        if (root != null) {
            TextView title = (TextView) root.findViewById(R.id.title);
            TextView desc = (TextView) root.findViewById(R.id.desc);
            ImageView image = (ImageView) root.findViewById(R.id.image);

            title.setText(cakes.get(position).getTitle());
            desc.setText(cakes.get(position).getDesc());
            //Glide lib
            mImageLoader.load(cakes.get(position).getImage(), image);
        }
        return root;
    }
}
