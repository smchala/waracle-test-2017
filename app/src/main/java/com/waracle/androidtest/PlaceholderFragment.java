package com.waracle.androidtest;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Fragment is responsible for loading in some JSON and
 * then displaying a list of cakes with images.
 * Fix any crashes
 * Improve any performance issues
 * Use good coding practices to make code more secure
 */
public class PlaceholderFragment extends ListFragment {


    private ListView mListView;
    private MyAdapter mAdapter;
    private static String JSON_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/" +
            "raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    public PlaceholderFragment() { /**/ }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mListView = (ListView) rootView.findViewById (android.R.id.list);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new GetCakesNow().execute();
    }

    private class GetCakesNow extends AsyncTask<Void, Void, ArrayList<Cake>> {
        @Override
        protected ArrayList<Cake> doInBackground(Void... params) {
            ArrayList<Cake> result = new ArrayList<>();
            try {
                result = loadData();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Cake> result) {
            mAdapter = new MyAdapter(getContext(), result);
            mListView.setAdapter(mAdapter);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private ArrayList<Cake> loadData() throws IOException, JSONException {
        //Retrofit +RXandroid/RXJava

        URL url = new URL(JSON_URL);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setUseCaches(true);
        int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
        urlConnection.addRequestProperty("Cache-Control", "max-stale=" + maxStale);
        InputStream in = null;
        try {
            in = new BufferedInputStream(urlConnection.getInputStream());

            // Can you think of a way to improve the performance of loading data
            // using HTTP headers?
            //caching:         urlConnection.setUseCaches(true);

            // Also, Do you trust any utils thrown your way????
            //using buffer
            byte[] bytes = StreamUtils.readUnknownFully(in);

            // Read in charset of HTTP content.
            String charset = parseCharset(urlConnection.getRequestProperty("Content-Type"));

            // Convert byte array to appropriate encoded string.
            String jsonText = new String(bytes, charset);

            // Read string as JSON.
            return convertArrayJsonToCakes(new JSONArray(jsonText));
        } finally {
            //StreamUtils.close(in);
            urlConnection.disconnect();
        }
    }

    private ArrayList<Cake> convertArrayJsonToCakes(JSONArray jsonArray) {
        //GSON
        ArrayList<Cake> cakes = new ArrayList<>();

        for (int i=0; i<jsonArray.length(); i++) {
            Cake cake = new Cake();
            JSONObject cakeJson = null;
            try {
                cakeJson = jsonArray.getJSONObject(i);
                cake.setTitle(cakeJson.getString("title"));
                cake.setDesc(cakeJson.getString("desc"));
                cake.setImage(cakeJson.getString("image"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            cakes.add(cake);
        }
        return cakes;
    }

    /**
     * Returns the charset specified in the Content-Type of this header,
     * or the HTTP default (ISO-8859-1) if none can be found.
     */
    public String parseCharset(String contentType) {
        if (contentType != null) {
            String[] params = contentType.split(",");
            for (int i = 1; i < params.length; i++) {
                String[] pair = params[i].trim().split("=");
                if (pair.length == 2) {
                    if (pair[0].equals("charset")) {
                        return pair[1];
                    }
                }
            }
        }
        return "UTF-8";
    }
}
